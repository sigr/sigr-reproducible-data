# Ecole thématique Sciences de l'information géographique reproductibles SIGR

[Ecole thématique CNRS](https://sigr2020.sciencesconf.org/resource/page/id/2), 27 juin au 2 juillet 2021 à Saint-Pierre d'Oléron. 

Ce markdown présente les données mobilisées lors de l'atelier **Outils de la recherche reproductible**, animé par Timothée Giraud, Ronan Ysebaert et Nicolas Lambert (UMS RIATE).

Il est accessible [ici](https://sigr.gitpages.huma-num.fr/sigr-reproducible-data)


